﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eriskitap.Models;
using System.Security.Cryptography;
using System.Text;
using eriskitap.DAL;

namespace eriskitap.Controllers
{
    public class UyesController : Controller
    {
        private eriskitapContext db = new eriskitapContext();

        // GET: Uyes
        [Filter.Admin]
        public ActionResult Index()
        {
            return View(db.Uyes.ToList());
        }

        // GET: Uyes
        [Filter.Admin]
        public ActionResult AdminPanel()
        {
            ViewBag.Son20Urun = db.Uruns.OrderByDescending(i => i.ID).Take(20).ToList();
            ViewBag.Son20Uye = db.Uyes.OrderByDescending(i => i.ID).Take(20).ToList();
            ViewBag.UrunSayisi = db.Uruns.Count();
            ViewBag.UyeSayisi = db.Uyes.Count();

            return View();
        }

        // GET: Uyes/Details/5
        [Filter.Admin]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Uye uye = db.Uyes.Find(id);
            if (uye == null)
            {
                return HttpNotFound();
            }
            return View(uye);
        }


        // GET: Uyes/Create
        public ActionResult UyeOl()
        {
            return View();
        }

        // POST: Uyes/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UyeOl(Uye uye)
        {
            uye.Sifre = eriskitap.Controllers.Fonksiyonlar.MD5Sifrele(uye.Sifre);
            uye.Rutbe = "Üye";
            if (ModelState.IsValid)
            {
                db.Uyes.Add(uye);
                db.SaveChanges();
                Session["Yetki"] = uye.Rutbe;
                Session["Username"] = uye.Ad;
                Session["uye_id"] = uye.ID;
                return RedirectToAction("Index");
            }

            return View(uye);
        }

        // GET: Uyes/Edit/5
        [Filter.Admin]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Uye uye = db.Uyes.Find(id);
            if (uye == null)
            {
                return HttpNotFound();
            }
            return View(uye);
        }

        // POST: Uyes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Filter.Admin]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Uye uye)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uye).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(uye);
        }

        // GET: Uyes/Delete/5
        [Filter.Admin]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Uye uye = db.Uyes.Find(id);
            if (uye == null)
            {
                return HttpNotFound();
            }
            return View(uye);
        }

        // POST: Uyes/Delete/5
        [Filter.Admin]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Uye uye = db.Uyes.Find(id);
            uye.Iletisim = db.Iletisims.Where(x => x.Uye.ID == uye.ID).ToList();
            db.Uyes.Remove(uye);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult CikisYap()
        {
            Session.Clear();
            return RedirectToAction("Index");
        }

        public ActionResult GirisYap()
        {
            if (Session["Yetki"] != null)
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        public ActionResult GirisYap(GirisModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            string tempSifre = eriskitap.Controllers.Fonksiyonlar.MD5Sifrele(model.Sifre as string);
            Uye uye = (from i in db.Uyes where i.Sifre == tempSifre && i.Eposta == model.Eposta select i).SingleOrDefault();
            if (uye != null)
            {
                Session["Yetki"] = uye.Rutbe;
                Session["Username"] = uye.Ad;
                Session["uye_id"] = uye.ID;

                if (uye.Rutbe == "Yönetici")
                    return RedirectToAction("AdminPanel");

                else
                    return RedirectToAction("Index");
            }
            ViewBag.uyari = "Lütfen bilgilerinizi kontrol ediniz !";
            return View();
        }

        public ActionResult Profil()
        {
            if (Session["sepet"] != null)
                ViewBag.Sepet = Session["sepet"] as List<Urun>;
            else
                ViewBag.Sepet = null;

            var uyeBilgi = db.Uyes.Find(Session["uye_id"]);

            var Adresler = db.Iletisims.Where(x => x.Uye.ID == uyeBilgi.ID).ToList();

            if (Adresler != null)
                ViewBag.Adresler = Adresler;
            else
                ViewBag.Adresler = null;

            ViewBag.UyeBilgi = uyeBilgi;
            return View();
        }
        public ActionResult AdresEkle()
        {
            if (Session["Yetki"] == null)
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        public ActionResult AdresEkle(Iletisim iletisim)
        {

            if (ModelState.IsValid)
            {
                iletisim.Uye = db.Uyes.Find(Session["uye_id"]);
                db.Iletisims.Add(iletisim);
                db.SaveChanges();
                return RedirectToAction("Profil");
            }

            return View();
        }
        public ActionResult Guncelle()
        {
            if (Session["uye_id"] == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var uye = db.Uyes.Find(Session["uye_id"]);
            if (uye == null)
            {
                return HttpNotFound();
            }
            return View(uye);
        }

        [HttpPost]
        public ActionResult Guncelle(Uye uye)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uye).State = EntityState.Modified;
                uye.Sifre = eriskitap.Controllers.Fonksiyonlar.MD5Sifrele(uye.Sifre);
                uye.Rutbe = (string)Session["Yetki"];
                db.SaveChanges();
                return RedirectToAction("Profil", "Uyes");
            }
            return View(uye);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
