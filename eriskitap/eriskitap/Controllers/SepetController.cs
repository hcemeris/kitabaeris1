﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eriskitap.Models;
using eriskitap.DAL;

namespace eriskitap.Controllers
{
    public class SepetController : Controller
    {
        private eriskitapContext db = new eriskitapContext();
        // GET: Sepet
        public ActionResult Index()
        {
            if (Session["uye_id"] == null)
                return RedirectToAction("GirisYap", "Uyes");
            List<Urun> listeDondur = new List<Urun>();
            if (Session["sepet"] == null)
            {
                return View(listeDondur);
            }
            listeDondur = (List<Urun>)Session["sepet"];
            return View(listeDondur);
        }

        public ActionResult SepetEkle(int id)
        {
            List<Urun> liste = new List<Urun>();
            Urun tmp = db.Uruns.Find(id);
            if (tmp == null)
            {
                return RedirectToAction("Index");
            }
            if (tmp.Adet == 0)
            {
                return Content("<script>alert('Bu üründen stokda kalmamıştır.');window.history.back(-1);</script>", "text/html");
            }
            if (Session["uye_id"] == null)
            {
                RedirectToAction("GirisYap", "Uyes");
            }
            if (Session["sepet"] != null)
                liste.AddRange((List<Urun>)Session["sepet"]);
            liste.Add(tmp);
            Session["sepet"] = liste;
            return RedirectToAction("Index");
        }
        public ActionResult Cikar(int id)
        {
            List<Urun> listeCikar = new List<Urun>();
            listeCikar = (List<Urun>)Session["sepet"];
            Urun tmp = listeCikar.Find(x => x.ID == id);
            if (tmp == null)
            {
                return RedirectToAction("Index");
            }
            listeCikar.Remove(tmp);
            Session["sepet"] = listeCikar;
            return RedirectToAction("Index");
        }
        public ActionResult SatinAl()
        {
            if (Session["uye_id"] == null)
            {
                return RedirectToAction("GirisYap", "Uyes");
            }
            if (Session["sepet"] == null)
            {
                return Content("<script>alert('Sepette satın alınacak ürün bulunmamaktadır.');window.location='/Uyes/Profil';</script>", "text/html");
            }
            List<Urun> listeSil = new List<Urun>();
            listeSil = (List<Urun>)Session["sepet"];
            Urun tmp;
            foreach (var i in listeSil)
            {
                tmp = db.Uruns.Find(i.ID);
                if (tmp.Adet >= 1)
                {
                    tmp.Adet--;
                    db.SaveChanges();
                }
                else
                    continue;
            }
            Session["sepet"] = null;
            return Content("<script>alert('Ürünler satın alındı.');window.location='/Sepet/Index';</script>", "text/html");
        }
    }
}