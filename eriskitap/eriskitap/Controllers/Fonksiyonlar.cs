﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace eriskitap.Controllers
{
    public class Fonksiyonlar
    {

        public static string MD5Sifrele(string str)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

            byte[] btr = Encoding.UTF8.GetBytes(str);
            btr = md5.ComputeHash(btr);

            StringBuilder sb = new StringBuilder();

            foreach (byte ba in btr)
            {
                sb.Append(ba.ToString("x2").ToLower());
            }

            return sb.ToString();
        }

        public static string URLUydur(string str)
        {
            if (string.IsNullOrEmpty(str))
                return "";

            if (str.Length > 80)
                str = str.Substring(0, 80);

            str = str.Replace("ş", "s");
            str = str.Replace("Ş", "S");
            str = str.Replace("ğ", "g");
            str = str.Replace("Ğ", "G");
            str = str.Replace("İ", "I");
            str = str.Replace("ı", "i");
            str = str.Replace("ç", "c");
            str = str.Replace("Ç", "C");
            str = str.Replace("ö", "o");
            str = str.Replace("Ö", "O");
            str = str.Replace("ü", "u");
            str = str.Replace("Ü", "U");
            str = str.Replace("'", "");
            str = str.Replace("\"", "");

            Regex r = new Regex("[^a-zA-Z0-9_-]");
            str = r.Replace(str, "-");
            if (!string.IsNullOrEmpty(str))
                while (str.IndexOf("--") > -1)
                    str = str.Replace("--", "-");
            if (str.StartsWith("-")) str = str.Substring(1);
            if (str.EndsWith("-")) str = str.Substring(0, str.Length - 1);
            return str;
        }
    }
}