namespace eriskitap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Iletisims",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Ad = c.String(nullable: false, maxLength: 25),
                        Sehir = c.String(nullable: false),
                        Semt = c.String(nullable: false, maxLength: 25),
                        Adres = c.String(nullable: false, maxLength: 150),
                        PostaKodu = c.String(nullable: false, maxLength: 25),
                        Telefon = c.String(nullable: false),
                        Uye_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Uyes", t => t.Uye_ID)
                .Index(t => t.Uye_ID);
            
            CreateTable(
                "dbo.Uyes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Ad = c.String(nullable: false, maxLength: 20),
                        Soyad = c.String(nullable: false, maxLength: 20),
                        Eposta = c.String(nullable: false),
                        Sifre = c.String(nullable: false),
                        Rutbe = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Uruns",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Ad = c.String(nullable: false),
                        Detay = c.String(nullable: false),
                        Kategori = c.String(nullable: false),
                        Fiyat = c.Double(nullable: false),
                        ResimYol = c.String(),
                        Adet = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Iletisims", "Uye_ID", "dbo.Uyes");
            DropIndex("dbo.Iletisims", new[] { "Uye_ID" });
            DropTable("dbo.Uruns");
            DropTable("dbo.Uyes");
            DropTable("dbo.Iletisims");
        }
    }
}
