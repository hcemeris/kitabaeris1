﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace eriskitap.Models
{
    public class Uye
    {
        public int ID { get; set; }

        [Required(ErrorMessageResourceType = typeof (eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenAdGir")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Lütfen adınızı 3-20 karakter uzunluğunda giriniz.")]
        public string Ad { get; set; }


        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenSoyadGir")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Lütfen soyadınızı 3-20 karakter uzunluğunda giriniz.")]
        public string Soyad { get; set; }

        [Display(Name = "E-posta")]

        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenEpostaGir")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Lütfen e-posta adresinizi geçerli bir formatta giriniz.")]
        public string Eposta { get; set; }

        [Display(Name = "Şifre")]

        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenSifreGir")]
        [DataType(DataType.Password)]
        public string Sifre { get; set; }

        [Display(Name = "Rütbe")]
        public string Rutbe { get; set; }

        public virtual List<Iletisim> Iletisim { get; set; }
    }
    public class GirisModel
    {

        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenEpostaGir")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Lütfen e-posta adresinizi geçerli bir formatta giriniz.")]
        public string Eposta { get; set; }


        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenSifreGir")]
        [DataType(DataType.Password)]
        public string Sifre { get; set; }
    }
}