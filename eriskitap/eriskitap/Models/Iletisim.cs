﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eriskitap.Models
{
    public class Iletisim
    {
        public int ID { get; set; }


        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenAdresAdi")]
        [StringLength(25, MinimumLength = 2, ErrorMessage = "Lütfen adres adınızı 2-25 karakter uzunluğunda giriniz.")]
        public string Ad { get; set; }

        [Display(Name = "Şehir")]

        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenSehir")]
        public string Sehir { get; set; }


        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenSemt")]
        [StringLength(25, MinimumLength = 2, ErrorMessage = "Lütfen semtinizi 2-25 karakter uzunluğunda giriniz.")]
        public string Semt { get; set; }


        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenAdres")]
        [StringLength(150, MinimumLength = 10, ErrorMessage = "Lütfen adresinizi 10-150 karakter uzunluğunda giriniz.")]
        public string Adres { get; set; }

        [Display(Name = "Posta Kodu")]

        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenSemtPosta")]
        [StringLength(25, MinimumLength = 3, ErrorMessage = "Lütfen posta kodunuzu 3-25 karakter uzunluğunda giriniz.")]
        public string PostaKodu { get; set; }


        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenTelNo")]
        public string Telefon { get; set; }

        public virtual Uye Uye { get; set; }
    }
}