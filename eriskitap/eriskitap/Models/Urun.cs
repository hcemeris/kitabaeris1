﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace eriskitap.Models
{
    public class Urun
    {
        public int ID { get; set; }


        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenUrunAdi")]
        public string Ad { get; set; }


        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenUrunDetayi")]
        public string Detay { get; set; }


        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenUrunKategorisi")]
        public string Kategori { get; set; }

        [Display(Name = "Fiyat (₺)")]

        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenUrunFiyati")]
        public double Fiyat { get; set; }

        [Display(Name = "Görsel")]
        [DataType(DataType.ImageUrl, ErrorMessage = "Lütfen resim yolunuzu doğru şekilde giriniz.")]
        public string ResimYol { get; set; }


        [Required(ErrorMessageResourceType = typeof(eriskitap.Resources.lang), ErrorMessageResourceName = "LutfenUrunAdet")]
        public int Adet { get; set; }
    }
}