﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using eriskitap.Models;

namespace eriskitap.DAL
{
    public class eriskitapBaslangicData : System.Data.Entity.DropCreateDatabaseIfModelChanges<eriskitapContext>
    {
        protected override void Seed(eriskitapContext context)
        {
            #region Ürünler

            var Urunler = new List<Urun>
            {
               

            };
            Urunler.ForEach(s => context.Uruns.Add(s));
            context.SaveChanges();
            
            #endregion


            #region Uyeler

            var Uyeler = new List<Uye>
            {
               
            };

            Uyeler.ForEach(s => context.Uyes.Add(s));
            context.SaveChanges();

            #endregion
        }
    }
}