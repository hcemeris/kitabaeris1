﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using eriskitap.Models;

namespace eriskitap.DAL
{
    public class eriskitapContext : DbContext
    {
        public eriskitapContext() : base("eriskitapContext") { }
        public DbSet<Uye> Uyes { get; set; }
        public DbSet<Urun> Uruns { get; set; }
        public DbSet<Iletisim> Iletisims { get; set; }
    }
}