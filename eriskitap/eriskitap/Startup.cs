﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(eriskitap.Startup))]
namespace eriskitap
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
