﻿using System.Web;
using System.Web.Mvc;
using eriskitap.Controllers;

namespace eriskitap
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
